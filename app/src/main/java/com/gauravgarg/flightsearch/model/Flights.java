package com.gauravgarg.flightsearch.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by gg on 27/09/17.
 */
public class Flights {

    //appendix object
    private Appendix appendix;

    //array of Flight
    private ArrayList<Flight> flights;


    public Appendix getAppendix() {
        return appendix;
    }

    public ArrayList<Flight> getFlights() {
        return flights;
    }

    public static class Appendix {
        private Map<String, String> airlines;
        private Map<String, String> airports;
        private Map<String, String> providers;

        public Map<String, String> getAirlines() {
            return airlines;
        }

        public Map<String, String> getAirports() {
            return airports;
        }

        public Map<String, String> getProviders() {
            return providers;
        }
    }

    public static class Flight {
        private String airlineCode;
        private String airlineName;
        private long arrivalTime;
        private long departureTime;
        private String originCode;
        private String originName;
        private String destinationCode;
        private String destinationName;
        @SerializedName("class")
        private String flightClass;
        private ArrayList<FareItem> fares;

        public String getAirlineCode() {
            return airlineCode;
        }

        public String getAirlineName() {
            return airlineName;
        }

        public void setAirlineName(String airlineName) {
            this.airlineName = airlineName;
        }

        public long getArrivalTime() {
            return arrivalTime;
        }

        public long getDepartureTime() {
            return departureTime;
        }

        public String getOriginCode() {
            return originCode;
        }

        public String getOriginName() {
            return originName;
        }

        public void setOriginName(String originName) {
            this.originName = originName;
        }

        public String getDestinationCode() {
            return destinationCode;
        }

        public String getDestinationName() {
            return destinationName;
        }

        public void setDestinationName(String destinationName) {
            this.destinationName = destinationName;
        }

        public String getFlightClass() {
            return flightClass;
        }

        public ArrayList<FareItem> getFares() {
            return fares;
        }

        public int getMinFare() {
            int min = fares.size() != 0 ? fares.get(0).getFare() : 0;
            for (FareItem fareItem : fares) {
                if (fareItem.getFare() < min) {
                    min = fareItem.getFare();
                }
            }
            return min;
        }

        public static class FareItem {
            private int fare;
            private int providerId;
            private String providerName;

            public int getFare() {
                return fare;
            }

            public int getProviderId() {
                return providerId;
            }

            public String getProviderName() {
                return providerName;
            }

            public void setProviderName(String providerName) {
                this.providerName = providerName;
            }
        }
    }
}
