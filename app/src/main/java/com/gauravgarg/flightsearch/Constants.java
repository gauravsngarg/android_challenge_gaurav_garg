package com.gauravgarg.flightsearch;

/**
 * Created by gg on 27/09/17.
 */
public class Constants {
    public static final String FLIGHT_API = "http://www.mocky.io/v2/5979c6731100001e039edcb3";

    public static final int SORT_BY_PRICE = 0;
    public static final int SORT_BY_LANDING = 1;
    public static final int SORT_BY_TAKEOFF = 2;
}
