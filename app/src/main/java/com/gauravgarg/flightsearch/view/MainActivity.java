package com.gauravgarg.flightsearch.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import com.gauravgarg.flightsearch.Constants;
import com.gauravgarg.flightsearch.R;
import com.gauravgarg.flightsearch.adapter.FlightAdapter;
import com.gauravgarg.flightsearch.databinding.ActivityMainBinding;
import com.gauravgarg.flightsearch.model.Flights;
import com.gauravgarg.flightsearch.viewmodel.MainViewModel;

import java.util.ArrayList;

/**
 * Created by gg on 27/09/17.
 */

public class MainActivity extends AppCompatActivity implements MainViewModel.IDataListener {

    private ActivityMainBinding binding;
    private MainViewModel mainViewModel;
    private FlightAdapter mFlightAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mainViewModel = new MainViewModel(this, this);
        binding.setViewModel(mainViewModel);
        mainViewModel.getFlightData();
    }

    @Override
    public void onDataFetched(ArrayList<Flights.Flight> flightItemArrayList) {
        //Set RecyclerView Adapter
        mFlightAdapter = new FlightAdapter(flightItemArrayList);
        binding.recyclerView.setAdapter(mFlightAdapter);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onDataFailure() {
        Snackbar.make(binding.recyclerView, getString(R.string.lbl_something_went_wrong), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onDataUpdate(int sortIndex) {
        //Update color of sort fields
        int activeColor = ContextCompat.getColor(this, R.color.colorAccent);
        int normalColor = ContextCompat.getColor(this, R.color.black);

        switch (sortIndex) {
            case Constants.SORT_BY_PRICE:
                binding.tvSortByPrice.setTextColor(activeColor);
                binding.tvSortByLanding.setTextColor(normalColor);
                binding.tvSortByTakeoff.setTextColor(normalColor);
                break;

            case Constants.SORT_BY_LANDING:
                binding.tvSortByPrice.setTextColor(normalColor);
                binding.tvSortByLanding.setTextColor(activeColor);
                binding.tvSortByTakeoff.setTextColor(normalColor);
                break;

            case Constants.SORT_BY_TAKEOFF:
                binding.tvSortByPrice.setTextColor(normalColor);
                binding.tvSortByLanding.setTextColor(normalColor);
                binding.tvSortByTakeoff.setTextColor(activeColor);
                break;

            default:
                break;
        }

        //Update data
        mFlightAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainViewModel.destroy();
    }
}
