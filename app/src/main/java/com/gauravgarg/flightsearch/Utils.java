package com.gauravgarg.flightsearch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by gg on 27/09/17.
 */
public class Utils {

    public static String getTime(long timestamp) {
        Date date = new Date();
        date.setTime(timestamp);
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mm", Locale.ENGLISH);
        return outputFormat.format(date);
    }

    public static String getDuration(long difference) {
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = difference / daysInMilli;
        difference = difference % daysInMilli;

        long elapsedHours = difference / hoursInMilli;
        difference = difference % hoursInMilli;

        long elapsedMinutes = difference / minutesInMilli;
//		different = different % minutesInMilli;

//		long elapsedSeconds = different / secondsInMilli;
        String result = "";
        if (elapsedDays == 1) {
            result += elapsedDays + " day ";
        } else if (elapsedDays > 1) {
            result += elapsedDays + " days ";
        }

        if (elapsedHours == 1) {
            result += elapsedHours + " hour ";
        } else if (elapsedHours > 1) {
            result += elapsedHours + " hours ";
        }

        if (elapsedMinutes == 1) {
            result += elapsedMinutes + " min ";
        } else if (elapsedMinutes > 1) {
            result += elapsedMinutes + " mins ";
        }
        return result;
    }
}
