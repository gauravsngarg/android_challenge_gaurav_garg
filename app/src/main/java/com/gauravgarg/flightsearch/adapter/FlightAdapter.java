package com.gauravgarg.flightsearch.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gauravgarg.flightsearch.R;
import com.gauravgarg.flightsearch.databinding.ItemFlightBinding;
import com.gauravgarg.flightsearch.model.Flights;
import com.gauravgarg.flightsearch.viewmodel.ItemFlightViewModel;

import java.util.ArrayList;

/**
 * Created by gg on 27/09/17.
 */

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {

    private ArrayList<Flights.Flight> mFlightsArrayList;

    public FlightAdapter(ArrayList<Flights.Flight> flightItemArrayList) {
        mFlightsArrayList = flightItemArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFlightBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_flight,
                parent,
                false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindFlight(mFlightsArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFlightsArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        final ItemFlightBinding binding;

        public ViewHolder(ItemFlightBinding binding) {
            super(binding.cardView);
            this.binding = binding;
        }

        void bindFlight(Flights.Flight flightItem) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new ItemFlightViewModel(itemView.getContext(), flightItem));
            } else {
                binding.getViewModel().setFlightItem(flightItem);
            }
        }
    }
}
