package com.gauravgarg.flightsearch.viewmodel;

/**
 * Interface that every ViewModel must implement
 */
public interface IViewModel {

    void destroy();
}
