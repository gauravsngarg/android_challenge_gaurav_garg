package com.gauravgarg.flightsearch.viewmodel;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.BaseObservable;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.gauravgarg.flightsearch.R;
import com.gauravgarg.flightsearch.Utils;
import com.gauravgarg.flightsearch.model.Flights;

/**
 * Created by gg on 27/09/17.
 */

public class ItemFlightViewModel extends BaseObservable implements IViewModel {
    private Context mContext;
    private Flights.Flight mFlightItem;

    public ItemFlightViewModel(Context context, Flights.Flight flightItem) {
        mContext = context;
        mFlightItem = flightItem;
    }

    public String getPrice() {
        return "\u20B9" + String.valueOf(mFlightItem.getMinFare());
    }

    /**
     * Show arrival and departure time
     *
     * @return
     */
    public String getTime() {
        return Utils.getTime(mFlightItem.getArrivalTime())
                + " - " +
                Utils.getTime(mFlightItem.getDepartureTime());
    }

    public String getFlightClass() {
        return mFlightItem.getFlightClass();
    }

    public String getAirlineName() {
        return mFlightItem.getAirlineName();
    }

    public String getAirportNames() {
        return mFlightItem.getOriginName() +
                " -> " +
                mFlightItem.getDestinationName();
    }

    public String getDuration() {
        return Utils.getDuration(mFlightItem.getArrivalTime() - mFlightItem.getDepartureTime());
    }

    /**
     * Show a list of all providers in a dialog box
     *
     * @param view
     */
    public void onItemClick(View view) {
        CharSequence[] arrChoice = new String[mFlightItem.getFares().size()];
        for (int i = 0; i < arrChoice.length; i++) {
            arrChoice[i] = mFlightItem.getFares().get(i).getProviderName() + " @ " + "\u20B9"
                    + mFlightItem.getFares().get(i).getFare();
        }

        new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.lbl_providers))
                .setSingleChoiceItems(arrChoice, 0, null)
                .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        // Do something useful withe the position of the selected radio button
                    }
                }).show();
    }

    /**
     * Allows recycling ItemFlightViewModels within the recyclerview adapter
     *
     * @param flightItem
     */
    public void setFlightItem(Flights.Flight flightItem) {
        mFlightItem = flightItem;
        notifyChange();
    }

    @Override
    public void destroy() {

    }
}
