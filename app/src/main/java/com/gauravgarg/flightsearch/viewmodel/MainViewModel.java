package com.gauravgarg.flightsearch.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.ObservableInt;
import android.view.View;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.gauravgarg.flightsearch.Constants;
import com.gauravgarg.flightsearch.model.Flights;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;

/**
 * Created by gg on 27/09/17.
 */

public class MainViewModel extends BaseObservable implements IViewModel {

    public ObservableInt progressVisibility;
    public ObservableInt recyclerViewVisibility;

    private Context mContext;
    private AsyncHttpClient mClient = new AsyncHttpClient();
    private ArrayList<Flights.Flight> mFlightItemArrayList;
    private IDataListener iDataListener;
    private int mCurrentSortedIndex = -1;

    public MainViewModel(Context context, IDataListener iDataListener) {
        mContext = context;
        this.iDataListener = iDataListener;

        progressVisibility = new ObservableInt(View.GONE);
        recyclerViewVisibility = new ObservableInt(View.GONE);
    }

    public void getFlightData() {
        progressVisibility.set(View.VISIBLE);
        recyclerViewVisibility.set(View.GONE);

        mClient.get(Constants.FLIGHT_API, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                progressVisibility.set(View.GONE);
                recyclerViewVisibility.set(View.VISIBLE);

                Flights flightItems = new Gson().fromJson(response.toString(), Flights.class);
                setFlightData(flightItems);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressVisibility.set(View.GONE);
                iDataListener.onDataFailure();
            }
        });
    }

    private void setFlightData(Flights flightItems) {
        Flights.Appendix appendix = flightItems.getAppendix();
        for (Flights.Flight flightItem : flightItems.getFlights()) {
            flightItem.setAirlineName(appendix.getAirlines().get(flightItem.getAirlineCode()));
            flightItem.setOriginName(appendix.getAirports().get(flightItem.getOriginCode()));
            flightItem.setDestinationName(appendix.getAirports().get(flightItem.getDestinationCode()));

            for (Flights.Flight.FareItem fareItem : flightItem.getFares()) {
                fareItem.setProviderName(appendix.getProviders().get(String.valueOf(fareItem.getProviderId())));
            }
        }
        mFlightItemArrayList = flightItems.getFlights();
        iDataListener.onDataFetched(mFlightItemArrayList);
    }

    public void onSortByPrice(View view) {
        sortBy(Constants.SORT_BY_PRICE);
    }

    public void onSortByLanding(View view) {
        sortBy(Constants.SORT_BY_LANDING);
    }

    public void onSortByTakeOff(View view) {
        sortBy(Constants.SORT_BY_TAKEOFF);
    }

    private void sortBy(final int sortIndex) {
        if (mCurrentSortedIndex == sortIndex) return;
        Collections.sort(mFlightItemArrayList, new Comparator<Flights.Flight>() {
            @Override
            public int compare(Flights.Flight o1, Flights.Flight o2) {
                int diff = 0;
                switch (sortIndex) {
                    case Constants.SORT_BY_PRICE:
                        diff = o1.getMinFare() - o2.getMinFare();
                        break;

                    case Constants.SORT_BY_LANDING:
                        diff = (int) (o1.getArrivalTime() - o2.getArrivalTime());
                        break;

                    case Constants.SORT_BY_TAKEOFF:
                        diff = (int) (o1.getDepartureTime() - o2.getDepartureTime());
                        break;

                    default:
                        break;
                }
                return diff;
            }
        });
        mCurrentSortedIndex = sortIndex;
        iDataListener.onDataUpdate(sortIndex);
    }

    @Override
    public void destroy() {
        mClient.cancelRequests(mContext, true);
    }

    public interface IDataListener {
        void onDataFetched(ArrayList<Flights.Flight> flightItemArrayList);

        void onDataFailure();

        void onDataUpdate(int sortIndex);
    }
}
